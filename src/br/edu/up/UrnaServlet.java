package br.edu.up;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.org.apache.xml.internal.security.utils.Base64;

import br.edu.up.dominio.Candidato;
import br.edu.up.dominio.Eleitor;
import br.edu.up.dominio.Urna;

@WebServlet("/cadastro")
public class UrnaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UrnaServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String listar = request.getParameter("filtro");

		response.setContentType("text/html");
	    PrintWriter out = response.getWriter();

	    out.println("<html>");
	    out.println("<head>");
	    out.println("<title>Urna Eletr�nica</title>");
	    out.println("</head>");
	    out.println("<body>");

		ServletContext ctx = getServletContext();
		String ctxPath = ctx.getRealPath("/WEB-INF/");	
		Urna urna = Urna.getInstancia(ctxPath);
		
		if ("eleitores".equals(listar)){
			List<Eleitor> eleitores = urna.getEleitores();
			out.println("<h2>Lista de Eleitores</h2>");
			out.println("<p><a href='/urna'><b>Voltar</b></a></p>");
			out.println("<table>");
			out.println("<tr><th>T�tulo</th><th>Nome</th></tr>");
			for (Eleitor eleitor : eleitores) {
				out.println("<tr><td>" + eleitor.getTitulo() + "</td><td>" + eleitor.getNome() + "</td></tr>");
			}
		}
		
		if ("candidatos".equals(listar)){
			List<Candidato> candidatos = urna.getCandidatos();
			out.println("<h2>Lista de Candidatos</h2>");
			out.println("<p><a href='/urna'><b>Voltar</b></a></p>");
			out.println("<table>");
			out.println("<tr>");
			for (Candidato candidato : candidatos) {
				String html = "<td>";
				html += "<b>C�digo:</b> " + candidato.getCodigo() + "<br />";
				html += "<b>Nome: </b>" + candidato.getNome() + "<br />";
				String foto = Base64.encode(candidato.getFoto());
				html += "<img src='data:image/jpg;base64," + foto + "' /><br />";
				html += "<b>Votos: <span style='color:red'>" + candidato.getVotos() + "</span></b>";
				html += "</td>";
				out.println(html);
			}
			out.println("</tr>");
			out.println("</table>");
		}
	    out.println("</body>");
	    out.println("</html>");	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
