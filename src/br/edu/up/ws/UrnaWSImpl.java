package br.edu.up.ws;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import br.edu.up.dominio.Candidato;
import br.edu.up.dominio.Cedula;
import br.edu.up.dominio.Urna;

@WebService(endpointInterface="br.edu.up.ws.UrnaWS")
public class UrnaWSImpl implements UrnaWS {
	
	@Resource
	private WebServiceContext wsctx;
	
	@Override
	public Cedula getCedula(String titulo) {
		return getUrna().getCedula(titulo);
	}

	@Override
	public Candidato getCandidato(String codigo) {
		return getUrna().getCandidato(codigo);
	}
	
	@Override
	public void votar(String titulo, String codigo) {
		Urna urna = getUrna();
		Cedula cedula = urna.getCedula(titulo);
		Candidato candidato = urna.getCandidato(codigo);
		cedula.setCandidato(candidato);
		urna.votar(cedula);
	}

	@Override
	public List<Candidato> getCandidatosClassificados() {
		return getUrna().getCandidatosClassificados();
	}
	
	private Urna getUrna() {
		ServletContext ctx = (ServletContext) wsctx.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		return Urna.getInstancia(ctx.getRealPath("/WEB-INF/"));
	}
}