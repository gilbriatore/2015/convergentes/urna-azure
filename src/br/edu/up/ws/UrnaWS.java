package br.edu.up.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;

import br.edu.up.dominio.Candidato;
import br.edu.up.dominio.Cedula;

@WebService
@SOAPBinding(style=Style.DOCUMENT, parameterStyle=ParameterStyle.WRAPPED)
public interface UrnaWS {

	@WebMethod
	Cedula getCedula(String titulo);

	@WebMethod 
	Candidato getCandidato(@WebParam(name="codigo") String codigo);

	@WebMethod
	void votar(String titulo, String codigo);

	@WebMethod
	List<Candidato> getCandidatosClassificados();
}