package br.edu.up.dominio;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Cedula implements Serializable {

	private static final long serialVersionUID = 7398049399423292018L;
	private Eleitor eleitor;
	private Candidato candidato;

	public Cedula() {
	}

	public Cedula(Eleitor eleitor) {
		this.eleitor = eleitor;
	}

	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
		this.candidato.addVoto();
	}

	public Candidato getCandidato() {
		return candidato;
	}

	public Eleitor getEleitor() {
		return eleitor;
	}

	public void setEleitor(Eleitor eleitor) {
		this.eleitor = eleitor;
	}
}