package br.edu.up.dominio;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class Candidato implements Serializable {

	private static final long serialVersionUID = 8274587681939966279L;
	private String codigo;
	private String nome;
	private byte[] foto;
	private int votos;

	public Candidato() {
	}

	public Candidato(String codigo, String nome, byte[] foto) {
		this.setCodigo(codigo);
		this.setNome(nome);
		this.foto = foto;
	}

	public void addVoto() {
		this.votos++;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	
	public void setVotos(int votos) {
		this.votos = votos;
	}

	public int getVotos() {
		return votos;
	}
}