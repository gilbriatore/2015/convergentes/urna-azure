package br.edu.up.dominio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Urna implements Serializable {

	static final Comparator<Candidato> ORDEM = new Comparator<Candidato>() {
		public int compare(Candidato c1, Candidato c2) {
			int valor = 0;
			valor = c2.getVotos() - c1.getVotos();
			return valor;
		}
	};

	private static final long serialVersionUID = 1696988090606969225L;
	private List<Cedula> votos;
	private Map<String, Candidato> candidatos;
	private Map<String, Eleitor> eleitores;
	private String ctxPath;
	
	private static Urna urna;
	
	public static Urna getInstancia(String ctxPath) {
		if (Urna.urna == null) {
			Urna.urna = new Urna(ctxPath);
		}
		return Urna.urna;
	}

	//Singleton
	private Urna(String ctxPath) {
		this.ctxPath = ctxPath;
		this.candidatos = carregarCandidatos();
		this.eleitores = carregarEleitores();
		this.votos = new ArrayList<Cedula>();
		Urna.urna = this;
	}

	private Map<String, Eleitor> carregarEleitores() {

		Map<String, Eleitor> eleitores = new HashMap<String, Eleitor>();
		try {
			File arquivo = new File(ctxPath + "/res/eleitores.csv");
			Scanner scan = new Scanner(arquivo);
			while (scan.hasNextLine()) {
				String linha = scan.nextLine();
				String[] dados = linha.split(";");
				eleitores.put(dados[0], new Eleitor(dados[0], dados[1]));
			}
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return eleitores;
	}

	private Map<String, Candidato> carregarCandidatos() {

		Map<String, Candidato> candidatos = new LinkedHashMap<String, Candidato>();
		try {
			File arquivo = new File(ctxPath + "/res/candidatos.csv");
			Scanner scan = new Scanner(arquivo);
			while (scan.hasNextLine()) {
				String linha = scan.nextLine();
				String[] dados = linha.split(";");
				byte[] foto = carregarFoto(dados[2]);
				candidatos.put(dados[0], new Candidato(dados[0], dados[1], foto));
			}

			byte[] nulo = carregarFoto("nulo.jpg");
			candidatos.put("33", new Candidato("33", "Nulo", nulo));

			byte[] branco = carregarFoto("branco.jpg");
			candidatos.put("44", new Candidato("44", "Em Branco", branco));

			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return candidatos;
	}

	private byte[] carregarFoto(String imagem) {
		Path pathRead = new File(ctxPath + "/res/" + imagem).toPath();
		byte[] foto = null;
		try {
			foto = Files.readAllBytes(pathRead);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return foto;
	}

	public void votar(Cedula cedula) {
		this.votos.add(cedula);
	}

	public Candidato getCandidato(String codigo) {
		return candidatos.get(codigo);
	}

	public Cedula getCedula(String titulo) {
		Cedula cedula = null;
		Eleitor eleitor = eleitores.get(titulo);
		if (eleitor != null) {
			cedula = new Cedula(eleitor);
		}
		return cedula;
	}

	public List<Eleitor> getEleitores() {
		return new ArrayList<Eleitor>(eleitores.values());
	}

	public List<Candidato> getCandidatos() {
		return new ArrayList<Candidato>(candidatos.values());
	}

	public List<Candidato> getCandidatosClassificados() {
		List<Candidato> lista = new ArrayList<Candidato>(candidatos.values());
		Collections.sort(lista, ORDEM);
		return lista;
	}
}