package br.edu.up.dominio;

import java.io.Serializable;
import java.rmi.RemoteException;

public class Eleitor implements Serializable {

	private static final long serialVersionUID = -4269673450569698233L;
	private String titulo;
	private String nome;

	public Eleitor() {
	}

	public Eleitor(String titulo, String nome) throws RemoteException {
		this.setTitulo(titulo);
		this.setNome(nome);
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}